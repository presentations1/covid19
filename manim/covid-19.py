#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" reported COVID-19 cases in Germany 
    data from https://datahub.io/core/covid-19#data
    wget https://datahub.io/core/covid-19/r/key-countries-pivoted.csv
"""

# from pandas_datapackage_reader import read_datapackage
from manimlib.imports import *
import numpy as np
import pandas as pd

source = "../data/key-countries-pivoted.csv"
c19_data = pd.read_csv(source, sep=",")

class EGrowthFormula(Scene):
    CONFIG = {
        "legend_scale": 0.75,
        "legend_color_map" : {
            '\\Delta': YELLOW,
            'N_d': YELLOW,
            'N_{d+1}': YELLOW,
            'E'  : BLUE,
            'p'  : GREEN
        },
        "surrounding_rectangle_config" : {
            'color' : RED,
        },
    }
    
    def construct(self):
        cases_symbol = TexMobject('N_d')
        cases_label = TextMobject(
            ':', 'Anzahl der best\\"{a}tigten F\\"{a}lle an einem bestimmten Tag')
        exposition_symbol = TexMobject('E')
        exposition_label = TextMobject(
            ':', 'Durchschnittliche Anzahl von Begegnungen jeden Tag')
        infectionP_symbol = TexMobject('p')
        infectionP_label = TextMobject(
            ':', 'Ansteckungswahrscheinlichkeit f\\"{u}r jede Begegnung')

        cases_symbol.set_color_by_tex_to_color_map(self.legend_color_map)
        exposition_symbol.set_color_by_tex_to_color_map(self.legend_color_map)
        infectionP_symbol.set_color_by_tex_to_color_map(self.legend_color_map)

        cases_symbol.scale(self.legend_scale)
        cases_label.scale(self.legend_scale)
        exposition_symbol.scale(self.legend_scale)
        exposition_label.scale(self.legend_scale)
        infectionP_symbol.scale(self.legend_scale)
        infectionP_label.scale(self.legend_scale)

        cases_symbol.to_corner(UP + LEFT)
        exposition_symbol.next_to(cases_symbol, DOWN, buff=MED_LARGE_BUFF)
        infectionP_symbol.next_to(exposition_symbol, DOWN, buff=MED_LARGE_BUFF)
        
        cases_label.next_to(cases_symbol, RIGHT)
        exposition_label.next_to(exposition_symbol, RIGHT)
        exposition_label.align_to(cases_label, LEFT)
        infectionP_label.next_to(infectionP_symbol, RIGHT)
        infectionP_label.align_to(cases_label, LEFT)

        formula_1 = TexMobject(r'\Delta', ' N_d', '=', 'E', r'\cdot', 'p', r'\cdot', 'N_d')
        formula_1.set_color_by_tex_to_color_map(self.legend_color_map)
        braces = Brace(VGroup(*formula_1[0:2]), DOWN)
        braces_text = braces.get_text(
            r'\"{A}nderung an einem Tag').scale(self.legend_scale).shift(0.3*UP)

        formula_2 = TexMobject(r'N_{d+1}', '=', 'N_d', '+', 'E', r'\cdot', 'p', r'\cdot', 'N_d')
        formula_2.set_color_by_tex_to_color_map(self.legend_color_map)
        formula_2[0].align_to(formula_1[1], RIGHT)
        formula_2[1].align_to(formula_1[2], LEFT)
        formula_2[2:].next_to(formula_2[1], RIGHT)
        formula_2.shift(LARGE_BUFF*DOWN)

        formula_3 = TexMobject(r'N_{d+1}', '=', '(', '1 + ', 'E', r'\cdot', 'p', r')\,', 'N_d')
        formula_3.set_color_by_tex_to_color_map(self.legend_color_map)
        formula_3[0].align_to(formula_1[1], RIGHT)
        formula_3[1].align_to(formula_1[2], LEFT)
        formula_3[2:].next_to(formula_1[2], RIGHT)
        formula_3.shift(2*LARGE_BUFF*DOWN)
        
        self.play(FadeInFrom(cases_symbol), FadeInFrom(cases_label))
        self.wait(3)
        self.play(FadeInFrom(exposition_symbol), FadeInFrom(exposition_label))
        self.wait(3)
        self.play(FadeInFrom(infectionP_symbol), FadeInFrom(infectionP_label))
        self.wait(3)

        # Formula 1
        self.add(VGroup(*formula_1[0:2]))
        self.play(GrowFromCenter(braces),
                  GrowFromCenter(braces_text))
        self.wait()
        self.play(GrowFromCenter(VGroup(*[formula_1[i] for i in [2, 4, 6]])),
                  LaggedStart(*[
                      TransformFromCopy(cases_symbol, formula_1[-1]),
                      TransformFromCopy(exposition_symbol, formula_1.get_part_by_tex("E")),
                      TransformFromCopy(infectionP_symbol, formula_1.get_part_by_tex("p"))]))
        self.wait()
        self.play(ShowCreationThenFadeAround(
            formula_1[-1],
            surrounding_rectangle_config=self.surrounding_rectangle_config,
            run_time=2))
        self.wait()

        # Formula 2
        self.play(FadeOut(braces), FadeOut(braces_text))
        self.play(TransformFromCopy(VGroup(*formula_1[1:3]), VGroup(*formula_2[:2])))
        self.play(FadeIn(VGroup(*formula_2[2:4])))
        self.play(TransformFromCopy(VGroup(*formula_1[2:]), VGroup(*formula_2[4:])))
        self.wait()

        # Formula_3
        self.play(TransformFromCopy(VGroup(*formula_2[:2]), VGroup(*formula_3[:2])))
        self.play(
            TransformFromCopy(formula_2[2], formula_3[8].copy(), remover=True),
            TransformFromCopy(formula_2[8], formula_3[8]))
        self.play(FadeIn(formula_3[3]))
        self.play(TransformFromCopy(VGroup(*formula_2[4:7]), VGroup(*formula_3[4:7])),
                  FadeIn(VGroup(*[formula_3[2], formula_3[7]])))
        self.wait(2)
        


class EGrowthGermany(GraphScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 70,
        "x_axis_label" : "",
        "x_labeled_nums" : [],
        "x_tick_frequency" : 7,
        "x_label_direction": DOWN,  # DR,
        "y_min" : 0,
        "y_max" : 1100,
        "y_axis_label" : "N",
        "y_tick_frequency" : 1e6,
        "y_labeled_nums" : [],
        "graph_origin" : 2.5*DOWN + 5*LEFT,
        "function_color" : YELLOW,
        "axes_color" : WHITE,
        "dates" : c19_data.loc[:,'Date'].to_numpy(),
        "cases" : c19_data.loc[:,'Germany'].to_numpy(),
        "date_scale_val": 0.6,
        "date_label_angle": 45,
        "number_scale_val": 0.75,
        "line_to_number_buff": MED_SMALL_BUFF,
        "numbers_with_elongated_ticks": range(0, 50, 7),
        "tick_size": 0.05,
        "longer_tick_multiple": 2,
        "grid_line_opacity": 0.5,
        "stroke_width": 2,
        "dot_color": YELLOW,
    }


    def set_x_axes_date_labels(self, animate=False):
        for number in range(self.i_start, self.i_stop, 7):
            label = self.dates[number][5:]
            lab_mob = TextMobject(label)
            lab_mob.scale(self.date_scale_val)
            lab_mob.rotate(self.date_label_angle)
            lab_mob.next_to(
                self.coords_to_point(number - self.i_start, 0),
                direction=self.x_label_direction,
                buff=self.line_to_number_buff
            )
            lab_mob.shift(0.15*LEFT)
            self.add(lab_mob)

    def y_grid_line(self, ypos, label):
        tick_size = self.tick_size * self.longer_tick_multiple
        tick = Line(tick_size*LEFT, tick_size*RIGHT, 
                    stroke_width = 2)
        tick.move_to(self.coords_to_point(0, ypos))
        # tick.match_style(self)
        lab_mob = TextMobject(label)
        lab_mob.scale(self.number_scale_val)
        lab_mob.next_to(
            self.coords_to_point(0, ypos),
            direction=LEFT,
            buff=self.line_to_number_buff)
        grid = Line(self.coords_to_point(0, ypos) + tick_size*RIGHT,
                    self.coords_to_point(self.x_max, ypos),
                    color = GRAY,
                    stroke_width = self.stroke_width,
                    opacity = self.grid_line_opacity)
        return VGroup(tick, lab_mob, grid)

    def construct(self):
        self.setup_axes(animate=False)
        self.set_x_axes_date_labels(animate=False)
        yaxes1 = VGroup(*[ self.y_grid_line(y, y) 
                           for y in range(200, 1001, 200)])
        yaxes2 = VGroup(*[ self.y_grid_line(y, y) 
                           for y in range(1000, 5001, 1000)])
        self.add(*yaxes1)

        dots = [SmallDot(self.coords_to_point(x, y), color=self.dot_color) 
                for (x, y) in enumerate(self.cases)]
        self.add(*dots[:self.min])
        for d in dots[self.x_min:48]:
            self.play(ShowCreation(d))
        self.play(FadeOutAndShiftDown(VGroup(*yaxes1)))
        
        self.wait(2)


class EGrowthLogIt(EGrowthGermany):
    CONFIG = {
        "i_stop": len(c19_data),
        "x_max" :    30,        # 20-03-31
        "y_max" : 110000,
    }
    
    def construct(self):
        self.i_start = self.i_stop - self.x_max
        self.setup_axes(animate=False)
        self.set_x_axes_date_labels(animate=False)
        title = TextMobject('Infektionszahlen Deutschland').scale(0.8)
        subtitle = TextMobject('Quelle: JHU CSSE').scale(0.4)
        title.shift(self.coords_to_point(15, 0.95*self.y_max))
        subtitle.next_to(title, DOWN)
        
        yaxes_n = VGroup(*[ self.y_grid_line(y, y)
                            for y in list(map(lambda x: x*20000, range(1,6)))])
        dots = [SmallDot(self.coords_to_point(x, y), color=self.dot_color) 
                for (x, y) in enumerate(self.cases[self.i_start:self.i_stop])]

        for d in dots:
            d.generate_target()
            d.target.arc_center[1] = np.log(d.arc_center[1]
            

        # linear plot
        self.add(*yaxes_n)
        self.add(title)
        self.add(subtitle)
        self.add(*dots)
        self.wait(2)

        # logarithmic plot
        
        
