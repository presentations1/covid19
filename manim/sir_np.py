#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" numerical integration of the SIR model in epidemiology """


import numpy as np
import pylab as p

from scipy import integrate


# initial conditions
N = 83.02e6
C = 150e3
H = 91.5e3
D = 4642

R0 = 1.3
tmax = 1000

gamma = 1/14
beta = R0*gamma

X0 = np.array([N - C, C - H - D, H + D])

# differential equations
def dX_dt(X, t=0):
    """ return the growth rates of the SIR model """
    S, I, R = X
    d1 = beta/N*S*I
    d2 = gamma*I

    return np.array([ -d1, d1 - d2, d2 ])

# integration
t = np.linspace(0, tmax, 10000)
X, infodict = integrate.odeint(dX_dt, X0, t, full_output=True)

S, I, R = X.T
f1 = p.figure()
p.plot(t, S, 'b-', label="S")
p.plot(t, I, 'r-', label="I")
p.plot(t, R, 'g-', label="R")
p.grid()
p.legend(loc='best')
p.xlabel('t/d')
p.ylabel('population')
p.title('COVID-19 — Germany')
f1.savefig('SIR_de_0.7.png')
f1.show()


