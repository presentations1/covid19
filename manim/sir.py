#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" SIR modeling of the Corona Virus Desease 2020 """

# from pandas_datapackage_reader import read_datapackage
from manimlib.imports import *

import numpy as np
from scipy import integrate

TH_RED    = "#C81E0F"
TH_ORANGE = "#EA5A00"
TH_VIOLET = "#B43092"

class Simulation():
    CONFIG = {                  # April 21st 2020
        "N"  : 83.02e6,         # populaton size of Germany (Wikipedia)
                                # Johns Hopkins University
                                # Coronavirus Resource Center
                                # https://coronavirus.jhu.edu/
        "C"  :  150000,         # reported cases
        "H"  :   95000,         # recovered (healed)
        "D"  :    5000,         # deceased
        "R0" :     0.7,         # basic reproduction number
        "L"  :      14,         # length of illness in days

        "segments" : 200,       # resolution of the graph
        "x_max"    : 209,       # simulation time (days)
    }

    def setup(self):
        # kinetic parameters
        gamma = 1/self.L
        beta = self.R0*gamma

        # initial conditions
        N = self.N
        C = self.C
        H = self.H
        D = self.D
        X0 = np.array([
            N - C,              # susceptible
            C - H - D,          # infectious
            H + D               # recovered (removed)
        ])

        def dX_dt(X, t=0):
            """ return the growth rates of the SIR model """
            S, I, R = X
            d1 = beta/N*S*I
            d2 = gamma*I
            
            return np.array([ -d1, d1 - d2, d2 ])

        # integration
        t = np.linspace(0, self.x_max, self.segments)
        X = integrate.odeint(dX_dt, X0, t, full_output=False)
        S, I, R, = X.T

        # publish
        self.beta  = beta
        self.gamma = gamma
        self.t     = t
        self.S     = S
        self.I     = I
        self.R     = R

    def show_R0(self, color=LIGHT_GRAY):
        legend = TexMobject("R_0", "=", str(self.R0).replace(".", "{,}"),
                            color=color)
        legend.move_to(2*UP + 3*RIGHT)
        self.play(Write(legend))

    def plot(self, x, y, color=TH_RED):
        for i in range(1, len(x)):
            line = Line(self.coords_to_point(x[i-1], y[i-1]), 
                        self.coords_to_point(x[i], y[i]),
                        color=color)
            self.add(line)
            self.wait(10/self.segments)


class StrogatzQuote(Scene):
    def construct(self):
        quote = self.get_quote()
        movers = VGroup(*quote[:-1].family_members_with_points())
        for mover in movers:
            mover.save_state()
            disc = Circle(radius=0.05)
            disc.set_stroke(width=0)
            disc.set_fill(BLACK, 0)
            disc.move_to(mover)
            mover.become(disc)
        self.play(
            FadeInFrom(quote.author_part, LEFT),
            LaggedStartMap(
                # FadeInFromLarge,
                # quote[:-1].family_members_with_points(),
                Restore, movers,
                lag_ratio=0.005,
                run_time=2,
            )
            # FadeInFromDown(quote[:-1]),
            # lag_ratio=0.01,
        )
        self.wait()
        self.play(
            Write(quote.law_part.copy().set_color(YELLOW)),
            run_time=1,
        )
        self.wait()
        self.play(
            Write(quote.language_part.copy().set_color(BLUE)),
            run_time=1.5,
        )
        self.wait(2)

    def get_quote(self):
        law_words = "laws of physics"
        language_words = "language of differential equations"
        author = "-Steven Strogatz"
        quote = TextMobject(
            """
            \\Large
            ``Since Newton, mankind has come to realize
            that the laws of physics are always expressed
            in the language of differential equations.''\\\\
            """ + author,
            alignment="",
            arg_separator=" ",
            substrings_to_isolate=[law_words, language_words, author]
        )
        quote.law_part = quote.get_part_by_tex(law_words)
        quote.language_part = quote.get_part_by_tex(language_words)
        quote.author_part = quote.get_part_by_tex(author)
        quote.set_width(12)
        quote.to_edge(UP)
        quote[-2].shift(SMALL_BUFF * LEFT)
        quote.author_part.shift(RIGHT + 0.5 * DOWN)
        quote.author_part.scale(1.2, about_edge=UL)

        return quote

class RussellQuote(Scene):
    def construct(self):
        quote = self.get_quote()
        movers = VGroup(*quote[:-1].family_members_with_points())
        for mover in movers:
            mover.save_state()
            disc = Circle(radius=0.05)
            disc.set_stroke(width=0)
            disc.set_fill(BLACK, 0)
            disc.move_to(mover)
            mover.become(disc)
        self.play(
            FadeInFrom(quote.author_part, LEFT),
            LaggedStartMap(
                # FadeInFromLarge,
                # quote[:-1].family_members_with_points(),
                Restore, movers,
                lag_ratio=0.005,
                run_time=2,
            )
            # FadeInFromDown(quote[:-1]),
            # lag_ratio=0.01,
        )
        self.wait()
        self.play(
            Write(quote.physics_part.copy().set_color(TH_VIOLET)),
            run_time=1,
        )
        self.wait()
        self.play(
            Write(quote.know_part.copy().set_color(TH_RED)),
            run_time=1.5,
        )
        self.wait(2)

    def get_quote(self):
        physics_words = "Physics is mathematical"
        know_words = "because we know so little."
        author = "-Bertrand Russell"
        quote = TextMobject(
            """
            \\Large
            ``Physics is mathematical not because we know so much
            about the physical world, but because we know so little.''\\\\
            """ + author,
            alignment="",
            arg_separator=" ",
            substrings_to_isolate=[physics_words, know_words, author]
        )
        quote.physics_part = quote.get_part_by_tex(physics_words)
        quote.know_part = quote.get_part_by_tex(know_words)
        quote.author_part = quote.get_part_by_tex(author)
        quote.set_width(12)
        quote.to_edge(UP)
        quote[1].shift(SMALL_BUFF * LEFT)
        quote[-2].next_to(quote[-3], RIGHT, buff=0).shift(0.14*UP)
        quote.author_part.shift(RIGHT + 0.5 * DOWN)
        quote.author_part.scale(1.2, about_edge=UL)

        return quote




class CompartmentModel(Scene):
    CONFIG = {
        "sir_color_map" : {
            'N' : GREEN,
            'C' : BLUE,
            'S' : TH_ORANGE,
            'I' : TH_RED,
            'R' : TH_VIOLET,
        },
        "surrounding_rectangle_config" : {
            'color' : RED,
        },
        "arrow_stroke_width": 3,
        "arrow_tip_length": 0.1,
    }

    def setup(self):
        compartments = ('S', 'I', 'R')
        self.rectangles = [ Rectangle(height= 1, width=1.6, color=self.sir_color_map[c])
                       for c in compartments ]
        self.labels = [ TexMobject(c, color=self.sir_color_map[c]) for c in compartments ]
            
        self.rectangles[1].to_edge(TOP)
        self.rectangles[0].next_to(self.rectangles[1], LEFT, buff=2)
        self.rectangles[2].next_to(self.rectangles[1], RIGHT, buff=2)

        self.edges = [ Arrow(self.rectangles[i].get_right() + 0.8*MED_SMALL_BUFF*LEFT,
                       self.rectangles[i+1].get_left() + 0.8*MED_SMALL_BUFF*RIGHT,
                       # buff=0,
                       stroke_width=self.arrow_stroke_width,
                       tip_length=self.arrow_tip_length,
                       color=LIGHT_GRAY) for i in range(2) ]

        self.diff_1 = TexMobject(r"\beta", "S", "I").next_to(self.edges[0], TOP, buff=0)
        self.diff_2 = TexMobject(r"\gamma", "I").next_to(self.edges[1], TOP, buff=0)

        self.transmission = TextMobject(r"$\beta$", ": Infektions-")
        self.recovery = TextMobject(r"$\gamma$", ": Genesungs-")
        self.rate = TextMobject("geschwindigkeitskonstante")

        self.transmission.scale(0.7).move_to(2*RIGHT)
        self.recovery.scale(0.7).move_to(2.07*RIGHT + .5*DOWN)
        self.rate.scale(0.7).to_edge(RIGHT).shift(1*DOWN)

        for t in [self.diff_1, self.diff_2]:
            t.scale(0.7).shift(0.1*DOWN)
            t.set_color_by_tex_to_color_map(self.sir_color_map)

        legend = VGroup(*[TextMobject("$S$", r": anf\"{a}llig (\textit{susceptible})"),
                          TextMobject("$I$", r": ansteckend (\textit{infectuous})"),
                          TextMobject("$R$", r": genesen (\textit{recovered})")])
        legend.arrange_submobjects(DOWN)
        for l in legend:
            l.align_to(self.rectangles[0].get_left(), LEFT)
            l.shift(DOWN)
            l.set_color_by_tex_to_color_map(self.sir_color_map)

        self.legend = legend

        
    def draw_compartments(self, legend=False):
        for i, (l, r) in enumerate(zip(self.labels, self.rectangles)):
            l.move_to(r.get_center())
            self.play(ShowCreation(r), FadeIn(l))
            if legend:
                self.play(TransformFromCopy(self.labels[i], self.legend[i][0]))
                self.play(Write(self.legend[i][1:]))

            if i < 2:
                self.play(ShowCreation(self.edges[i]))

    def draw_transitions(self, show_legend=False):
        self.play(TransformFromCopy(self.labels[0], self.diff_1[1]))
        self.wait(2)
        self.play(TransformFromCopy(self.labels[1], self.diff_1[2]))
        self.wait(1)
        self.play(FadeInFrom(self.diff_1[0], UP))
        self.wait()
        self.play(TransformFromCopy(self.labels[1], self.diff_2[1]))
        self.play(FadeInFrom(self.diff_2[0], UP))
        if show_legend:
            self.wait(2)
            self.play(Write(self.transmission))
            self.play(Write(self.recovery))
            self.play(Write(self.rate))

    def construct(self):
        self.draw_compartments(legend=True)
        self.wait(4)

class DifferentialEquations(CompartmentModel):
    def setup(self):
        super().setup()
        deq_s_mob = TexMobject(
            r"{d", "S", r"\over dt}", 
            "=", "-", r"\beta", "S", "I",
            tex_to_color_map=self.sir_color_map
        )
        deq_i_mob = TexMobject(
            r"{d", "I", r"\over dt}", 
            "=", "+", r"\beta", "S", "I", 
            "-", r"\gamma", "I", 
            tex_to_color_map=self.sir_color_map
        )
        deq_r_mob = TexMobject(
            r"{d", "R", r"\over dt}", 
            "=", r"\hphantom{-}", r"\beta", "S", "I", 
            r"+",  r"\gamma", "I", 
            tex_to_color_map=self.sir_color_map
        )
        for (s, i, r) in zip(deq_s_mob, deq_i_mob, deq_r_mob):
            s.align_to(i, LEFT)
            r.align_to(i, LEFT)
        self.deq_s = VGroup(*deq_s_mob).align_to(self.rectangles[0].get_left(), LEFT)
        self.deq_i = VGroup(*deq_i_mob[:4], *deq_i_mob[5:]).align_to(self.rectangles[0].get_left(), LEFT)
        self.deq_r = VGroup(*deq_r_mob[:4], *deq_r_mob[9:]).align_to(self.rectangles[0].get_left(), LEFT)
        self.deq_i.shift(1.3*DOWN)
        self.deq_r.shift(2.6*DOWN)
                                          

    def draw_deqs(self):
        self.play(TransformFromCopy(self.labels[0], self.deq_s[1]),
                  TransformFromCopy(self.labels[1], self.deq_i[1]),
                  TransformFromCopy(self.labels[2], self.deq_r[1]),
                  )
        self.play(Write(VGroup(*[ self.deq_s[i] for i in (0, 2, 3)])),
                  Write(VGroup(*[ self.deq_i[i] for i in (0, 2, 3)])),
                  Write(VGroup(*[ self.deq_r[i] for i in (0, 2, 3)])),
                  )
        self.wait(3)
        self.play(TransformFromCopy(self.diff_1, self.deq_s[5:8], run_time=2),
                  TransformFromCopy(self.diff_1, self.deq_i[4:7], run_time=2),
                  Write(self.deq_s[4])
                  )
        self.wait(5)
        self.play(TransformFromCopy(self.diff_2, self.deq_i[8:], run_time=2),
                  TransformFromCopy(self.diff_2, self.deq_r[4:], run_time=2),
                  Write(self.deq_i[7])
                  )

    def construct(self):
        self.draw_compartments()
        self.wait(2)
        self.draw_transitions()
        self.wait(2)
        self.draw_deqs()
        self.wait(6)

        
class InitialConditions(CompartmentModel):
    def construct(self):
        title = TextMobject("Anfangsbedingungen:").to_corner(UP+LEFT)

        n_label = TexMobject(r'N: \mbox{Gesamtbev\"{o}lkerung}',
                             tex_to_color_map=self.sir_color_map)
        c_label = TexMobject(r'C: \text{best\"{a}tigte F\"{a}lle}',
                             tex_to_color_map=self.sir_color_map)
        r_label = TexMobject(r"R: \text{Genesene und Verstorbene}",
                             tex_to_color_map=self.sir_color_map)
        
        n  = TexMobject("N", "=", "S", "+", "I", "+", "R", 
                        r"\approx 83{,}02\cdot 10^6",
                        r"\quad\mbox{(DE)}",
                        tex_to_color_map=self.sir_color_map)
        c  = TexMobject("C", "=", "I" + "R",
                        tex_to_color_map=self.sir_color_map)
        

        n_label.next_to(title, DOWN, buff=MED_LARGE_BUFF).to_edge(LEFT).shift(RIGHT)
        n.next_to(n_label, DOWN).to_edge(LEFT).shift(RIGHT)
        c_label.next_to(n, DOWN).to_edge(LEFT).shift(RIGHT)
        r_label.next_to(c_label, DOWN).to_edge(LEFT).shift(RIGHT)

        si = TexMobject("S", "{}_i", "=" "N - C", tex_to_color_map=self.sir_color_map)
        ii = TexMobject("I", "{}_i", "=" "C - R", tex_to_color_map=self.sir_color_map)
        ri = TexMobject("R", "{}_i", "=" "R", tex_to_color_map=self.sir_color_map)

        si.next_to(r_label, DOWN, buff=LARGE_BUFF).to_edge(LEFT).shift(1.06*RIGHT)
        ii.next_to(si, DOWN).to_edge(LEFT).shift(1.11*RIGHT)
        ri.next_to(ii, DOWN).to_edge(LEFT).shift(RIGHT)

        self.play(Write(title))
        self.wait(2)
        self.play(Write(n_label))
        self.play(Write(n))
        self.wait(2)
        self.play(Write(c_label))
        self.play(Write(r_label))
        self.wait(2)
        self.play(Write(si))
        self.wait(1)
        self.play(Write(ii))
        self.wait(1)
        self.play(Write(ri))
        self.wait(4)

        
class SIRPlot07(GraphScene, Simulation):
    CONFIG = {
        "y_min" :   0,
        "y_max" :  61,
        "x_min" :   0,
        "x_max" : 209,
        "x_tick_frequency"  : 10,
        "y_tick_frequency"  : 10,
        "x_axis_label"      : "$t$/d",
        "y_axis_label"      : "$10^3$",
        "x_labeled_nums"    : range(0, 209, 50),
        "y_labeled_nums"    : range(0, 65, 10),
        "x_label_decimal"   : 0,
        "y_label_decimal"   : 0,
        "x_label_direction" : LEFT,
        "x_label_direction" : DOWN,
    }

    def setup(self):
        Simulation.setup(self)
        GraphScene.setup(self)
    
    def construct(self):
        self.setup_axes(animate=True)
        self.show_R0
        self.plot(self.t, self.I/1000)
        self.wait(3)

        
class SIRPlot11(SIRPlot07):
    CONFIG = {
        "R0"               : 1.1,           # basic reproduction number

        "y_max"            : 401,
        "x_max"            : 1009,
        "x_tick_frequency" : 50,
        "y_tick_frequency" : 10,
        "x_labeled_nums"   : range(0, 1001, 100),
        "y_labeled_nums"   : range(0, 1009, 50),
    }

class SIRPlot11R(SIRPlot11):
    CONFIG = {
        "y_max"            : 15,
        "y_tick_frequency" : 1,
        "y_labeled_nums"   : range(0, 16, 5),
        "y_axis_label"     : "$10^6$",
    }
    def construct(self):
        self.setup_axes(animate=True)
        self.show_R0()
        self.plot(self.t, self.I/1e6)
        self.plot(self.t, self.R/1e6, color=TH_VIOLET)
        self.wait(3)

class SIRPlot13R(SIRPlot11):
    CONFIG = {
        "R0"               : 1.3,           # basic reproduction number
        "x_max"            : 500,
        "x_tick_frequency" : 10,
        "x_labeled_nums"   : range(0, 501, 50),
        "y_max"            : 85,
        "y_tick_frequency" : 5,
        "y_labeled_nums"   : range(0, 86, 10),
        "y_axis_label"     : "$10^6$",
    }
    def construct(self):
        # modus = self.coords_to_point(250, 2.5)
        # ptr = Arrow(self.coords_to_point(300, 4),
        #             self.coords_to_point(250, 2.5))
        Imax = TexMobject(r"2.5\cdot 10^6").scale(0.6)
        Imax.move_to(2.1*DOWN + 0.5*RIGHT)
        self.setup_axes(animate=True)
        self.show_R0()
        self.plot(self.t, self.S/1e6, color=TH_ORANGE)
        self.plot(self.t, self.I/1e6)
        self.plot(self.t, self.R/1e6, color=TH_VIOLET)
        self.wait()
        self.play(FadeIn(Imax))
        self.wait(3)
