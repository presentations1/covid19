This project originates in exercise sheet 19 for the module *Pharmaceutics* in the course
*Drug Discovery and Development* at [TH Köln](https:/th-koeln.de).

# Introduction

2020 is the year of the [coronavirus
pandemic](https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic); universities were closed down, and until the means for online-teaching could be established, there was great demand for self study exercises. 

The epidemiology of any pandemic is based on [first order
kinetics](https://en.wikipedia.org/wiki/Exponential_growth) and the
[logistic differential
equation](https://en.wikipedia.org/wiki/Logistic_function#Logistic_differential_equation). It
is thus not specific to the module Pharmaceutics, but thinking about
it can provide the students valuable insights into the underlying principles which
are fundamental to the natural sciences. En passant they can aquire some
useful soft skills.

# Getting the data

In order to work with data we need data. Reliable and up-to-date
sources for COVID19 cases are the [Johns Hopkins University Center for
Systems Science and Engineering (JHU CSSE)](https://systems.jhu.edu/)
for worldwide data and the [Robert Koch
Institute](https://en.wikipedia.org/wiki/Robert_Koch_Institute) for
case data in Germany.

## Data sources

### JHU CSSE

Getting the Johns Hopkins data is almost too easy: They provide
up-to-date data in csv-format on
[GitHub](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series). Data are provided for confimed cases and deaths and are updated every day.

### Robert Koch Institute

Getting the RKI data is slightly more involved, since the data is
provided on a human readable
[webpage](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html)
on a day by day basis.

The solution is to
[scrape](https://en.wikipedia.org/wiki/Web_scraping) the website every
day by means of a [cron](https://en.wikipedia.org/wiki/Cron) job.

## Web scraping

Scraping websites is an useful technique for gathering data. Excellent
examples for scraping are provided by [David Kriesel](https://www.dkriesel.com/start):

- [SpiegelMinig – Reverse Enginneering von Spiegel-Online (33c3)](https://www.youtube.com/watch?v=-YpwsdRKt8Q)
- [BahnMining – Pünktlichkeit ist eine Zier (36c3)](https://www.youtube.com/watch?v=0rb9CfOvojk)

### Scrapy

[Python](https:www.python.org) provides an extraordinally powerful web
scraping tool named [scrapy](https://scrapy.org/). Using scrapy boils
down to programming a *spider* that crawls websites and extracts the
data you are interested in.


![RKI Webpage mit Fallzahlen](images/rki_2020-03-25.png)


The spider takes just a few lines of python code:

```python
# -*- coding: utf-8 -*-
import scrapy
import re, datetime

class RkiSpider(scrapy.Spider):
    name = 'rki'
    allowed_domains = ['www.rki.de']
    start_urls = ['http://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html/']

    def get_date(self, response):
        state = response.css('p.null::text').get()
        self.logger.info(state)
        match = re.search('Stand: (\d+)\.(\d+)\.(\d+),\s+(\d+):(\d+)\s+Uhr', state)
        d, m, y, hh, mm = map(int, match.groups())
        return datetime.datetime(y, m, d, hh, mm)

    def parse(self, response):
        today = self.get_date(response)
        # read the table
        keys = ['country', 'total', 'diff', 'per1e5', 'deaths']
        for row in response.css('table > tbody > tr'):
            data = row.css('td::text').getall()
            result = dict(zip(keys, data))
            result['date'] = today
            yield result
```


## dathub.io

Another means of getting the date in a computer readable format is
[databub.io](https://datahub.io/core/covid-19). There are, for
instance, condensed data of cumulative total confirmed cases to date
for key countries (including Germany):

<iframe src="https://datahub.io/core/covid-19/r/1.html" width="100%" height="100%" frameborder="0"></iframe>

# Playing with the data

## Exponential growth

During the initial phase the number of infected persons grows
[exponentially](https://en.wikipedia.org/wiki/Exponential_growth):

```math
N(t) = N_0\cdot e^{k\cdot t}
```

There is a [video](https://www.youtube.com/watch?v=Kas0tIxDvrg&t=43s)
about exponential growth in epidemcis on the excellent
[3blu1brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw)
channel on [youtube](https://www.youtube.com):


<a href="http://www.youtube.com/watch?feature=player_embedded&v=Kas0tIxDvrg" target="_blank"><img src="http://img.youtube.com/vi/Kas0tIxDvrg/0.jpg" 
alt="Exponential growth and epidemics" width="240" height="180" border="10" /></a>



### growth constant

The *growth constant* $`k`$ is of interest here because it determines
how rapid the numbers grow. It is tightly related to the
*reduplication time* $`T`$:

```math
T = \frac{\ln 2}{k}
```





## Logistic differential equation

# Visualising the data

# Digitalisation

## value of M. Sc. and Ph. D.
