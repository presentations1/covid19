# -*- coding: utf-8 -*-
import scrapy
import re, datetime

class RkiSpider(scrapy.Spider):
    name = 'rki'
    allowed_domains = ['www.rki.de']
    start_urls = ['http://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html/']

    def get_date(self, response):
        state = response.css('#main p::text').get()
        self.logger.info(state)
        match = re.search('Stand: (\d+)\.(\d+)\.(\d+),\s+(\d+):(\d+)\s+Uhr', state)
        d, m, y, hh, mm = map(int, match.groups())
        return datetime.datetime(y, m, d, hh, mm)

    def parse(self, response):
        today = self.get_date(response)
        # read the table
        keys = ['country', 'total', 'diff', 'per1e5', 'deaths']
        for row in response.css('table > tbody > tr'):
            data = row.css('td::text').getall()
            result = dict(zip(keys, data))
            result['date'] = today
            yield result
