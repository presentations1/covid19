{"country": "Baden-W\u00fcrttem\u00adberg", "total": "6.069", "diff": "+721", "per1e5": "55", "deaths": "37", "date": "2020-03-25 00:00:00"}
{"country": "Bayern", "total": "6.558", "diff": "+804", "per1e5": "50", "deaths": "37", "date": "2020-03-25 00:00:00"}
{"country": "Berlin", "total": "1.428", "diff": "+208", "per1e5": "38", "deaths": "2", "date": "2020-03-25 00:00:00"}
{"country": "Brandenburg", "total": "429", "diff": "+86", "per1e5": "17", "deaths": "1", "date": "2020-03-25 00:00:00"}
{"country": "Bremen", "total": "200", "diff": "+17", "per1e5": "29", "date": "2020-03-25 00:00:00"}
{"country": "Hamburg", "total": "1.262", "diff": "+219", "per1e5": "69", "date": "2020-03-25 00:00:00"}
{"country": "Hessen", "total": "1.754", "diff": "+134", "per1e5": "28", "deaths": "4", "date": "2020-03-25 00:00:00"}
{"country": "Mecklenburg-Vor\u00adpommern", "total": "218", "diff": "+19", "per1e5": "14", "date": "2020-03-25 00:00:00"}
{"country": "Niedersachsen", "total": "2.140", "diff": "+376", "per1e5": "27", "deaths": "7", "date": "2020-03-25 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "7.197", "diff": "+879", "per1e5": "40", "deaths": "43", "date": "2020-03-25 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "1.591", "diff": "+221", "per1e5": "39", "deaths": "5", "date": "2020-03-25 00:00:00"}
{"country": "Saarland", "total": "403", "diff": "+66", "per1e5": "41", "deaths": "2", "date": "2020-03-25 00:00:00"}
{"country": "Sachsen", "total": "959", "diff": "+148", "per1e5": "24", "deaths": "6", "date": "2020-03-25 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "375", "diff": "+54", "per1e5": "17", "deaths": "1", "date": "2020-03-25 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "577", "diff": "+99", "per1e5": "20", "deaths": "3", "date": "2020-03-25 00:00:00"}
{"country": "Th\u00fcringen", "total": "394", "diff": "+67", "per1e5": "18", "deaths": "1", "date": "2020-03-25 00:00:00"}
{"country": "31.554", "total": "+4.118", "diff": "38", "per1e5": "149", "date": "2020-03-25 00:00:00"}
{"country": "Baden-W\u00fcrttem\u00adberg", "total": "7.283", "diff": "+1.214", "per1e5": "66", "deaths": "56", "date": "2020-03-26 00:00:00"}
{"country": "Bayern", "total": "7.993", "diff": "+1.435", "per1e5": "61", "deaths": "47", "date": "2020-03-26 00:00:00"}
{"country": "Berlin", "total": "1.656", "diff": "+228", "per1e5": "44", "deaths": "4", "date": "2020-03-26 00:00:00"}
{"country": "Brandenburg", "total": "477", "diff": "+48", "per1e5": "19", "deaths": "1", "date": "2020-03-26 00:00:00"}
{"country": "Bremen", "total": "211", "diff": "+11", "per1e5": "31", "deaths": "1", "date": "2020-03-26 00:00:00"}
{"country": "Hamburg", "total": "1.265", "diff": "+3*", "per1e5": "69", "date": "2020-03-26 00:00:00"}
{"country": "Hessen", "total": "2.157", "diff": "+403", "per1e5": "34", "deaths": "6", "date": "2020-03-26 00:00:00"}
{"country": "Mecklenburg-Vor\u00adpommern", "total": "244", "diff": "+26", "per1e5": "15", "date": "2020-03-26 00:00:00"}
{"country": "Niedersachsen", "total": "2.347", "diff": "+207", "per1e5": "29", "deaths": "8", "date": "2020-03-26 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "7.924", "diff": "+727", "per1e5": "44", "deaths": "53", "date": "2020-03-26 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "1.816", "diff": "+225", "per1e5": "44", "deaths": "6", "date": "2020-03-26 00:00:00"}
{"country": "Saarland", "total": "433", "diff": "+30", "per1e5": "44", "deaths": "2", "date": "2020-03-26 00:00:00"}
{"country": "Sachsen", "total": "1.141", "diff": "+182", "per1e5": "28", "deaths": "7", "date": "2020-03-26 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "418", "diff": "+43", "per1e5": "19", "deaths": "1", "date": "2020-03-26 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "675", "diff": "+98", "per1e5": "23", "deaths": "3", "date": "2020-03-26 00:00:00"}
{"country": "Th\u00fcringen", "total": "468", "diff": "+74", "per1e5": "22", "deaths": "3", "date": "2020-03-26 00:00:00"}
{"country": "36.508", "total": "+4.954", "diff": "44", "per1e5": "198", "date": "2020-03-26 00:00:00"}
{"country": "Baden-W\u00fcrttem\u00adberg", "total": "8.161", "diff": "+878", "per1e5": "74", "deaths": "70", "date": "2020-03-27 00:00:00"}
{"country": "Bayern", "total": "9.481", "diff": "+1.488", "per1e5": "73", "deaths": "55", "date": "2020-03-27 00:00:00"}
{"country": "Berlin", "total": "1.955", "diff": "+299", "per1e5": "52", "deaths": "8", "date": "2020-03-27 00:00:00"}
{"country": "Brandenburg", "total": "537", "diff": "+60", "per1e5": "21", "deaths": "1", "date": "2020-03-27 00:00:00"}
{"country": "Bremen", "total": "241", "diff": "+30", "per1e5": "35", "deaths": "1", "date": "2020-03-27 00:00:00"}
{"country": "Hamburg", "total": "1.693", "diff": "+428*", "per1e5": "92", "deaths": "2", "date": "2020-03-27 00:00:00"}
{"country": "Hessen", "total": "2.323", "diff": "+166", "per1e5": "37", "deaths": "7", "date": "2020-03-27 00:00:00"}
{"country": "Mecklenburg-Vor\u00adpommern", "total": "259", "diff": "+15", "per1e5": "16", "date": "2020-03-27 00:00:00"}
{"country": "Niedersachsen", "total": "2.810", "diff": "+463", "per1e5": "35", "deaths": "10", "date": "2020-03-27 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "9.235", "diff": "+1.311", "per1e5": "51", "deaths": "72", "date": "2020-03-27 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "1.971", "diff": "+155", "per1e5": "48", "deaths": "8", "date": "2020-03-27 00:00:00"}
{"country": "Saarland", "total": "505", "diff": "+72", "per1e5": "51", "deaths": "2", "date": "2020-03-27 00:00:00"}
{"country": "Sachsen", "total": "1.305", "diff": "+164", "per1e5": "32", "deaths": "7", "date": "2020-03-27 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "458", "diff": "+40", "per1e5": "21", "deaths": "2", "date": "2020-03-27 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "812", "diff": "+137", "per1e5": "28", "deaths": "4", "date": "2020-03-27 00:00:00"}
{"country": "Th\u00fcringen", "total": "542", "diff": "+74", "per1e5": "25", "deaths": "4", "date": "2020-03-27 00:00:00"}
{"country": "42.288", "total": "+5.780", "diff": "51", "per1e5": "253", "date": "2020-03-27 00:00:00"}
{"country": "Baden-W\u00fcrttem\u00adberg", "total": "9.781", "diff": "+1.620", "per1e5": "88", "deaths": "101", "date": "2020-03-28 00:00:00"}
{"country": "Bayern", "total": "11.150", "diff": "+1.669", "per1e5": "85", "deaths": "77", "date": "2020-03-28 00:00:00"}
{"country": "Berlin", "total": "2.161", "diff": "+206", "per1e5": "58", "deaths": "8", "date": "2020-03-28 00:00:00"}
{"country": "Brandenburg", "total": "645", "diff": "+108", "per1e5": "26", "deaths": "1", "date": "2020-03-28 00:00:00"}
{"country": "Bremen", "total": "260", "diff": "+19", "per1e5": "38", "deaths": "2", "date": "2020-03-28 00:00:00"}
{"country": "Hamburg", "total": "1.765", "diff": "+72", "per1e5": "96", "deaths": "2", "date": "2020-03-28 00:00:00"}
{"country": "Hessen", "total": "2.604", "diff": "+281", "per1e5": "42", "deaths": "9", "date": "2020-03-28 00:00:00"}
{"country": "Mecklenburg-Vor\u00adpommern", "total": "308", "diff": "+49", "per1e5": "19", "date": "2020-03-28 00:00:00"}
{"country": "Niedersachsen", "total": "3.150", "diff": "+340", "per1e5": "39", "deaths": "12", "date": "2020-03-28 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "10.607", "diff": "+1.372", "per1e5": "59", "deaths": "80", "date": "2020-03-28 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "2.212", "diff": "+241", "per1e5": "54", "deaths": "11", "date": "2020-03-28 00:00:00"}
{"country": "Saarland", "total": "550", "diff": "+45", "per1e5": "56", "deaths": "2", "date": "2020-03-28 00:00:00"}
{"country": "Sachsen", "total": "1.432", "diff": "+127", "per1e5": "35", "deaths": "9", "date": "2020-03-28 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "458", "diff": "+0 *", "per1e5": "21", "deaths": "2", "date": "2020-03-28 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "915", "diff": "+103", "per1e5": "32", "deaths": "4", "date": "2020-03-28 00:00:00"}
{"country": "Th\u00fcringen", "total": "584", "diff": "+42", "per1e5": "27", "deaths": "5", "date": "2020-03-28 00:00:00"}
{"country": "48.582", "total": "+6.294", "diff": "58", "per1e5": "325", "date": "2020-03-28 00:00:00"}
{"country": "Baden-W\u00fcrttem\u00adberg", "total": "10.943", "diff": "+1.149", "per1e5": "99", "deaths": "119", "date": "2020-03-30 00:00:00"}
{"country": "Bayern", "total": "13.989", "diff": "+1.108", "per1e5": "107", "deaths": "127", "date": "2020-03-30 00:00:00"}
{"country": "Berlin", "total": "2.464", "diff": "+104", "per1e5": "66", "deaths": "11", "date": "2020-03-30 00:00:00"}
{"country": "Brandenburg", "total": "761", "diff": "+40", "per1e5": "30", "deaths": "2", "date": "2020-03-30 00:00:00"}
{"country": "Bremen", "total": "286", "diff": "+11", "per1e5": "42", "deaths": "2", "date": "2020-03-30 00:00:00"}
{"country": "Hamburg", "total": "2.053", "diff": "+207", "per1e5": "112", "deaths": "5", "date": "2020-03-30 00:00:00"}
{"country": "Hessen", "total": "3.091", "diff": "+486", "per1e5": "49", "deaths": "13", "date": "2020-03-30 00:00:00"}
{"country": "Mecklenburg-Vor\u00adpommern", "total": "356", "diff": "+8", "per1e5": "22", "deaths": "1", "date": "2020-03-30 00:00:00"}
{"country": "Niedersachsen", "total": "3.732", "diff": "+282", "per1e5": "47", "deaths": "26", "date": "2020-03-30 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "12.178", "diff": "+778", "per1e5": "68", "deaths": "101", "date": "2020-03-30 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "2.584", "diff": "+188", "per1e5": "63", "deaths": "18", "date": "2020-03-30 00:00:00"}
{"country": "Saarland", "total": "706", "diff": "+146", "per1e5": "71", "deaths": "7", "date": "2020-03-30 00:00:00"}
{"country": "Sachsen", "total": "1.795", "diff": "+178", "per1e5": "44", "deaths": "9", "date": "2020-03-30 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "592", "diff": "+0*", "per1e5": "27", "deaths": "2", "date": "2020-03-30 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "1.049", "diff": "+44", "per1e5": "36", "deaths": "7", "date": "2020-03-30 00:00:00"}
{"country": "Th\u00fcringen", "total": "719", "diff": "+22", "per1e5": "34", "deaths": "5", "date": "2020-03-30 00:00:00"}
{"country": "57.298", "total": "+4.751", "diff": "69", "per1e5": "455", "date": "2020-03-30 00:00:00"}
{"country": "Baden-W\u00fcrttem\u00adberg", "total": "13.410", "diff": "+1.076", "per1e5": "121", "deaths": "197", "date": "2020-04-01 00:00:00"}
{"country": "Bayern", "total": "16.497", "diff": "+1.687", "per1e5": "126", "deaths": "225", "date": "2020-04-01 00:00:00"}
{"country": "Berlin", "total": "2.754", "diff": "+179", "per1e5": "73", "deaths": "16", "date": "2020-04-01 00:00:00"}
{"country": "Brandenburg", "total": "881", "diff": "+83", "per1e5": "35", "deaths": "4", "date": "2020-04-01 00:00:00"}
{"country": "Bremen", "total": "311", "diff": "+17", "per1e5": "46", "deaths": "5", "date": "2020-04-01 00:00:00"}
{"country": "Hamburg", "total": "2.311", "diff": "+120", "per1e5": "126", "deaths": "14", "date": "2020-04-01 00:00:00"}
{"country": "Hessen", "total": "3.445", "diff": "+162", "per1e5": "55", "deaths": "21", "date": "2020-04-01 00:00:00"}
{"country": "Mecklenburg-", "total": "\nVor\u00adpommern", "diff": "406", "per1e5": "+40", "deaths": "25", "date": "2020-04-01 00:00:00"}
{"country": "Niedersachsen", "total": "4.382", "diff": "+319", "per1e5": "55", "deaths": "42", "date": "2020-04-01 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "14.351", "diff": "+1.126", "per1e5": "80", "deaths": "134", "date": "2020-04-01 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "2.899", "diff": "+173", "per1e5": "71", "deaths": "23", "date": "2020-04-01 00:00:00"}
{"country": "Saarland", "total": "829", "diff": "+47", "per1e5": "84", "deaths": "8", "date": "2020-04-01 00:00:00"}
{"country": "Sachsen", "total": "2.034", "diff": "+152", "per1e5": "50", "deaths": "17", "date": "2020-04-01 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "750", "diff": "+70", "per1e5": "34", "deaths": "7", "date": "2020-04-01 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "1.246", "diff": "+126", "per1e5": "43", "deaths": "10", "date": "2020-04-01 00:00:00"}
{"country": "Th\u00fcringen", "total": "860", "diff": "+76", "per1e5": "40", "deaths": "6", "date": "2020-04-01 00:00:00"}
{"country": "67.366", "total": "+5.453", "diff": "81", "per1e5": "732", "date": "2020-04-01 00:00:00"}
{"country": "Baden-W\u00fcrttem\u00adberg", "total": "14.662", "diff": "+1.252", "per1e5": "132", "deaths": "241", "date": "2020-04-02 00:00:00"}
{"country": "Bayern", "total": "18.496", "diff": "+1.999", "per1e5": "141", "deaths": "268", "date": "2020-04-02 00:00:00"}
{"country": "Berlin", "total": "2.970", "diff": "+216", "per1e5": "79", "deaths": "19", "date": "2020-04-02 00:00:00"}
{"country": "Brandenburg", "total": "995", "diff": "+114", "per1e5": "40", "deaths": "7", "date": "2020-04-02 00:00:00"}
{"country": "Bremen", "total": "327", "diff": "+16", "per1e5": "48", "deaths": "6", "date": "2020-04-02 00:00:00"}
{"country": "Hamburg", "total": "2.406", "diff": "+95", "per1e5": "131", "deaths": "14", "date": "2020-04-02 00:00:00"}
{"country": "Hessen", "total": "3.707", "diff": "+262", "per1e5": "59", "deaths": "27", "date": "2020-04-02 00:00:00"}
{"country": "Mecklenburg-", "total": "\nVor\u00adpommern", "diff": "438", "per1e5": "+32", "deaths": "27", "date": "2020-04-02 00:00:00"}
{"country": "Niedersachsen", "total": "4.695", "diff": "+313", "per1e5": "59", "deaths": "45", "date": "2020-04-02 00:00:00"}
{"country": "Nordrhein-West\u00adfalen", "total": "15.427", "diff": "+1.076", "per1e5": "86", "deaths": "161", "date": "2020-04-02 00:00:00"}
{"country": "Rhein\u00adland-Pfalz", "total": "3.132", "diff": "+233", "per1e5": "77", "deaths": "26", "date": "2020-04-02 00:00:00"}
{"country": "Saarland", "total": "1.020", "diff": "+191", "per1e5": "103", "deaths": "11", "date": "2020-04-02 00:00:00"}
{"country": "Sachsen", "total": "2.183", "diff": "+149", "per1e5": "54", "deaths": "18", "date": "2020-04-02 00:00:00"}
{"country": "Sachsen-Anhalt", "total": "804", "diff": "+54", "per1e5": "36", "deaths": "8", "date": "2020-04-02 00:00:00"}
{"country": "Schles\u00adwig-Holstein", "total": "1.335", "diff": "+89", "per1e5": "46", "deaths": "11", "date": "2020-04-02 00:00:00"}
{"country": "Th\u00fcringen", "total": "925", "diff": "+65", "per1e5": "43", "deaths": "7", "date": "2020-04-02 00:00:00"}
{"country": "73.522", "total": "+6.156", "diff": "88", "per1e5": "872", "date": "2020-04-02 00:00:00"}
