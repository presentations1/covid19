#! /bin/bash

today=$(date +"%Y_%m_%d")

lkdata=RKI-LK-$today.csv 
bldata=RKI-BL-$today.csv
kcdata=key-countries-pivoted.csv
lklog=03-landkreise.log
bllog=02-bundeslaender.log
kclog=01-key-countries.log

lk=https://opendata.arcgis.com/datasets/917fc37a709542548cc3be077a786c17_0.csv
bl=https://opendata.arcgis.com/datasets/ef4b445a53c1406892257fe63129a8ea_0.csv
kc=https://datahub.io/core/covid-19/r/$kcdata

wget -a $lklog -O $lkdata $lk
wget -a $bllog -O $bldata $bl
wget -N -a $kclog $kc

git add $lkdata $bldata $kcdata
git add $lklog $bllog $kclog
git commit -m "cron data update $today"
git push
