#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" confirmed COVID19 cases in Germany """

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import io, requests

from scipy import stats

# online = True                  # get data directly out of the internet
online = False                 # work with local copy of the data

url = "https://datahub.io/core/covid-19/r/key-countries-pivoted.csv"
source = "../data/key-countries-pivoted_csv.csv"

if online:
    response = requests.get(url)
    source = io.StringIO(response.content.decode('utf-8'))

df = pd.read_csv(source, sep=",")


df.plot(x='Date')
plt.show()

# df.loc[30:].plot(x='Date', subplots=True, kind='scatter')
df.loc[35:].plot(x='Date')
plt.yscale('log')
plt.show()

gf = df[['Date', 'Germany']]
gmar = gf[gf['Date'] >= '2020-03-01']


gmar.plot(style='bo')
plt.yscale('log')
plt.grid(True)
plt.legend()
plt.show()

gmar.loc[51:58].plot(x='Date')
plt.grid(True)
plt.yscale('log')
plt.show()



# growth factors

a = df.iloc[1:,1:].reset_index(drop=True)   # skip first row
b = df.iloc[:-1,1:].reset_index(drop=True)  # skip last row
d = df.loc[1:,'Date'].reset_index(drop=True) # Date column
gf = pd.concat([d, a/b], axis=1)

gf.loc[51:,['Date','Germany']].plot('Date', 'Germany', kind='scatter')
plt.show()


# growth factors in Germany at the end of March 2020
gf_de = gf.loc[51:,['Date','Germany']]
split = gf_de['Germany'] < 1.2
# velocity constants
k1 = np.log(gf_de.loc[~split, 'Germany']).mean()
k2 = np.log(gf_de.loc[split, 'Germany']).mean()
# growth factors
f1 = np.exp(k1)
f2 = np.exp(k2)

gf.loc[51:,['Date','Germany']].plot('Date', 'Germany', kind='scatter')
plt.plot([0, 6], [f1, f1], 'r-')
plt.plot([7, 12], [f2, f2], 'r-')
plt.show()


# reduplicaton times
T1 = np.log(2.0)/k1
T2 = np.log(2.0)/k2

# Extrapolation
## case k1

## case k2
